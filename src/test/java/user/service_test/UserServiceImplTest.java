package user.service_test;

import am.egs.exam.config.jwt.JwtProvider;
import am.egs.exam.model.entity.Role;
import am.egs.exam.model.entity.User;
import am.egs.exam.repository.UserRepository;
import am.egs.exam.service.impl.UserServiceImpl;
import am.egs.exam.util.exceptions.DuplicateException;
import javassist.NotFoundException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

/**
* User Test
 * @author tirayrs
* */


@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    JwtProvider jwtProvider;


    @InjectMocks
    UserServiceImpl userService = new UserServiceImpl();

    private User user;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        user = new User();
        user.setName("Tiro");
        user.setSurname("Sulyan");
        user.setAge(25);
        user.setEmail("tiro.sulyan@mail.ru");
        user.setPassword("tiropass");

    }

    @After
    public void destroy() {
        userRepository.delete(user);
    }


    /*   Test
     *   User Registration
     * */
    @Test
    public void add() throws MessagingException {
        Mockito.when(userRepository.save(user))
                .thenAnswer((Answer<User>) invocation -> {
                    User user2 = (User) invocation.getArguments()[0];
                    user2.setId(1L);
                    return user2;
                });
        Assert.assertNull(user.getId());


        try {
            userService.add(user);
        } catch (DuplicateException e) {
            e.printStackTrace();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        Assert.assertNotNull(user.getId());

        System.out.println(user);

    }


    /*   Test
     *   Get User By Email
     * */
    @Test
    public void getByEmail() throws NotFoundException, MessagingException {

        add();

        Mockito.when(userRepository.getUserByEmail(user.getEmail())).thenReturn(user);

        User resultUser = userService.getByEmail(user.getEmail());

        Assert.assertNotNull(user.getId());
        Assert.assertEquals("Tiro", resultUser.getName());
        Assert.assertEquals("Sulyan", resultUser.getSurname());
        Assert.assertEquals(25, resultUser.getAge());
        Assert.assertEquals("tiro.sulyan@mail.ru", resultUser.getEmail());
        Assert.assertEquals("tiropass", resultUser.getPassword());

    }


    /*   Test
     *   Create Token With Email and Roles
     * */
    @Test
    public void getToken() throws MessagingException {
        add();

        String email = "tiro.sulyan@mail.ru";

        Role role = new Role("ROLE_USER");
        List<Role> roles = new ArrayList<>();
        roles.add(role);

        String token = userService.getToken(email, roles);
        System.out.println(token);

        String resultToken = userService.getToken(email, roles);
        System.out.println(resultToken);

        Assert.assertNotNull(resultToken);
        Assert.assertEquals(token, resultToken);
    }




}
