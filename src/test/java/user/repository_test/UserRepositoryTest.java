package user.repository_test;

import am.egs.exam.model.entity.User;
import am.egs.exam.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@EnableAutoConfiguration
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserRepository.class)

public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    private User user;

    @Before
    public void init() {
        user = new User();
        user.setName("Tiro");
        user.setSurname("Sulyan");
        user.setAge(25);
        user.setEmail("tiro.sulyan@mail.ru");
        user.setPassword("tiropass");

    }

    @Test
    public void add() {

        System.out.println(user);

        User user1 = userRepository.save(user);

        Assert.assertNotNull(user1);
        Assert.assertEquals("Tiro", user1.getName());


    }
}
