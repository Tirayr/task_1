package car.service_test;


import am.egs.exam.model.entity.Car;
import am.egs.exam.repository.CarRepository;
import am.egs.exam.service.CarService;
import am.egs.exam.service.impl.CarServiceImpl;
import javassist.NotFoundException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 * User Test
 *
 * @author tirayrs
 */

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    @Mock
    private CarRepository carRepository;

    @InjectMocks
    CarService carService = new CarServiceImpl();

    private Car car;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        car = new Car();
        car.setModel("Toyota");
        car.setMarka("Corola");
        car.setCreateYear(2008);

    }

    @After
    public void destroy() {
        carRepository.delete(car);
    }

//    @Test
//    public void addTest() {
//        Mockito.when(carRepository.save(car)).thenAnswer((Answer<Car>) invocation -> {
//            Car car = (Car) invocation.getArguments()[0];
//            car.setId(1L);
//            return car;
//        });
//        Assert.assertNull(car.getId());
//        try {
//            carService.add(car, file, token);
//        } catch (NotFoundException e) {
//            e.printStackTrace();
//        }
//        Assert.assertNotNull(car.getId());
//        System.out.println(car);
//
//
//    }

}
