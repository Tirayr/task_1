package am.egs.exam.config.security;

import am.egs.exam.config.jwt.JwtConfig;
import am.egs.exam.config.jwt.JwtFilter;
import am.egs.exam.config.jwt.JwtProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    private final JwtProvider jwtProvider;

    public SecurityConfig(JwtProvider jwtProvider, JwtFilter jwtFilter) {
        this.jwtProvider = jwtProvider;
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
                .and()
                .authorizeRequests()
                .antMatchers("/user/**").permitAll()// access for ROLE_USER and ROLE_ADMIN
                .antMatchers("/user/update").authenticated()
                .antMatchers("/image/**").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN") // access for only ROLE_ADMIN
                .antMatchers("/car/**").permitAll()
                .antMatchers("/car/add").authenticated()
                .antMatchers("/chat/**").permitAll()
                .antMatchers("/secured/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfig(jwtProvider));


       /* http
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .antMatcher("/book/**")
                .authenticationProvider(jwtProvider)
                .authorizeRequests()
                .anyRequest().authenticated();*/


    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth
                .authenticationProvider(jwtProvider);
    }
}
