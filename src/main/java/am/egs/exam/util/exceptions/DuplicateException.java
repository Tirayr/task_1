package am.egs.exam.util.exceptions;

public class DuplicateException extends Throwable {
    public DuplicateException(String message) {
        super(message);
    }
}
