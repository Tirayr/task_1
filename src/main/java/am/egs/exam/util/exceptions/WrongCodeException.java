package am.egs.exam.util.exceptions;

public class WrongCodeException extends Throwable{
    public WrongCodeException(String message) {
        super(message);
    }
}
