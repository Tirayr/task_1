package am.egs.exam.util.exceptions;

public class RoleNotFoundException extends Throwable {
    public RoleNotFoundException(String message) {
        super(message);
    }
}
