package am.egs.exam.util.exceptions;

public class NullPointerException extends Exception {
    public NullPointerException(String message) {
        super(message);
    }
}
