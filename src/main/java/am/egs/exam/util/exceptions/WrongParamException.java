package am.egs.exam.util.exceptions;

public class WrongParamException extends Throwable {
    public WrongParamException(String message) {
        super(message);

    }
}
