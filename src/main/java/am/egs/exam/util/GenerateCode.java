package am.egs.exam.util;


import am.egs.exam.model.entity.User;

import java.util.Random;

public class GenerateCode {

    private static final char[] NUMBERS = "0123456789".toCharArray();

    public static String generateCode(){
        int count = 7;
        StringBuilder builder = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < count ; i++) {
            char c = NUMBERS[random.nextInt(NUMBERS.length)];
            builder.append(c);
        }
        return builder.toString();
    }

    public static String getVerificationMessage(User user){
        return "Hello dear " + user.getName().substring(0,1).toUpperCase() + user.getName().substring(1) +
                " You were successfully registered. Please verify. Your Verification code is: " + user.getCode();
    }



}
