package am.egs.exam.model.entity;

import org.hibernate.annotations.Fetch;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Component
public class Car {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String model;

    @NotNull
    private String marka;

    @NotNull
    private int createYear;

    @NotNull
    private String ownerContact;

    @NotNull
    private String vinCode;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "car_image",
            joinColumns = @JoinColumn(name = "car_id"),
            inverseJoinColumns = @JoinColumn(name = "image_id"))
    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getVinCode() {
        return vinCode;
    }

    public void setVinCode(String vinCode) {
        this.vinCode = vinCode;
    }

    public String getOwnerContact() {
        return ownerContact;
    }

    public void setOwnerContact(String ownerContact) {
        this.ownerContact = ownerContact;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public int getCreateYear() {
        return createYear;
    }

    public void setCreateYear(int createYear) {
        this.createYear = createYear;
    }

    public Car() {

    }

    public Car(@NotNull String model, @NotNull String marka,
               @NotNull int createYear, @NotNull String ownerContact,
               @NotNull String vinCode, List<Image> images) {
        this.model = model;
        this.marka = marka;
        this.createYear = createYear;
        this.ownerContact = ownerContact;
        this.vinCode = vinCode;
        this.images = images;
    }
}
