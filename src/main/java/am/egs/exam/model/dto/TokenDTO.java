package am.egs.exam.model.dto;

import java.io.Serializable;

public class TokenDTO implements Serializable {
    private final String token;

    public TokenDTO() {
        this.token = null;
    }

    public TokenDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
