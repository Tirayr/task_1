package am.egs.exam.model.dto;

public class ImageDto {

   private String imageString;

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public ImageDto(){

    }

    public ImageDto(String imageString) {
        this.imageString = imageString;
    }

    @Override
    public String toString() {
        return "ImageDto{" +
                "imageString='" + imageString + '\'' +
                '}';
    }
}
