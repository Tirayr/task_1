package am.egs.exam.model.user_state;

public enum State {
    ONLINE,
    OFFLINE,
    ONCHAT;
}
