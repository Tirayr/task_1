package am.egs.exam.controller;


import am.egs.exam.model.entity.Chat;
import am.egs.exam.model.entity.User;
import am.egs.exam.service.ChatService;
import am.egs.exam.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.Date;

@Controller
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private UserService userService;

    @MessageMapping("/secured/hello/{recipientEmail}")
    @SendTo("/secured/topic/greetings/{recipientEmail}")
    public String greeting(Chat chat, @PathParam("recipientEmail") String recipientEmail) throws NotFoundException, IOException {
        String recipientUserEmail = chat.getHeader();
        Date now = new Date();
        User user = userService.getByEmail(chat.getSenderEmail());
        String content = chat.getContent();
        String contentForView  = now.toString() + "\n" + user.getName() + " " + user.getSurname() + " :   " + chat.getContent();
        chatService.setNotification(recipientUserEmail);
        chatService.saveMessage(chat.getSenderEmail(), chat.getHeader(), content, now);
        return contentForView;
    }



}
