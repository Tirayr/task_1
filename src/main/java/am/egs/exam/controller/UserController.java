package am.egs.exam.controller;


import am.egs.exam.config.jwt.JwtProvider;
import am.egs.exam.model.dto.TokenDTO;
import am.egs.exam.model.dto.UserDto;
import am.egs.exam.model.entity.Chat;
import am.egs.exam.model.entity.User;
import am.egs.exam.service.ChatService;
import am.egs.exam.service.UserService;
import am.egs.exam.util.exceptions.DuplicateException;
import am.egs.exam.util.exceptions.WrongCodeException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private AuthenticationManager authenticationManager;


    @PostMapping(value = "/registration")
    public ResponseEntity registration(@RequestBody @Valid User user) throws MessagingException {
        try {
            userService.add(user);
        } catch (DuplicateException e) {
            return new ResponseEntity("User is exist !!!", HttpStatus.valueOf(409));
        } catch (NotFoundException e) {
            return new ResponseEntity("User not found !!!", HttpStatus.valueOf(404));
        }
        return ResponseEntity.ok().build();
    }

    /*
     *    Verify
     * */
    @PostMapping("/verify")
    public ResponseEntity verify(@RequestParam("email") String email,
                                 @RequestParam("code") String code) {
        try {
            userService.verify(email, code);
        } catch (NotFoundException e) {
            return new ResponseEntity("There is no user with this email", HttpStatus.valueOf(404));
        } catch (WrongCodeException e) {
            return new ResponseEntity("Wrong verification code", HttpStatus.valueOf(400));
        }
        return ResponseEntity.ok().build();

    }

    /*
     *   Login
     * */
    @ResponseBody
    @PostMapping("/login")
    public Object login(@RequestParam("email") String email,
                        @RequestParam("password") String password) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));

        User user = null;
        try {
            user = userService.getByEmail(email);
        } catch (NotFoundException e) {
            return new ResponseEntity("There is no user with this email", HttpStatus.valueOf(404));
        }

        if (!password.equals(user.getPassword())) {
            return new ResponseEntity("Wrong email or password !!!", HttpStatus.valueOf(400));
        }

        String token = userService.getToken(email, user.getRoles());

        TokenDTO tokenDTO = new TokenDTO(token);

        return tokenDTO;
    }

    @GetMapping("/getUser")
    public ResponseEntity getUser(@RequestHeader("token") String token) {
        Boolean isAdmin = userService.getUserByToken(token);
        return ResponseEntity.ok(isAdmin);
    }

    /*
     *   Update User
     * */
    @PostMapping("/update")
    public ResponseEntity update(@RequestHeader("token") String token,
                                 @RequestBody User user) {
        System.out.println(token);
        try {
            userService.update(user, token);
        } catch (AccessDeniedException e) {
            return new ResponseEntity("you can't update data of other users", HttpStatus.valueOf(400));
        } catch (NotFoundException e) {
            return new ResponseEntity("user doesn't exist !!!", HttpStatus.valueOf(404));
        }

        return ResponseEntity.ok().build();
    }


    /*
     *    FINDE USER
     * */
    @GetMapping("/getAll")
    List<User> findAll() {
        return userService.getAllUsers();
    }

    /*
     *   Token Validation
     * */
    @GetMapping("/validateToken")
    public boolean isValid(@RequestHeader("token") String token) {
        System.out.println(token);
        if (token == null || !jwtProvider.validateToken(token)) {
            return false;
        }
        return true;
    }

    /*
     *   Find user by email
     * */
    @GetMapping("/findUser")
    public UserDto findUser(@RequestHeader("token") String token) {
        UserDto userDto = userService.findUserByEmail(token);
        return userDto;
    }

    /*
     *   Get User For Chat
     * */
    @GetMapping("/getUsersForChat")
    public UserDto getUsers(@RequestParam("vinCode") String vinCode) {
        UserDto userDto = userService.getUserByCarVinCode(vinCode);
        return userDto;
    }

    /*
     *   Log Out
     * */
    @GetMapping("/logOut")
    public void logOut(@RequestHeader("token") String token) {
        userService.logOut(token);
    }

    /*
     *    Has Message
     * */
    @GetMapping("/hasMessage")
    public Boolean hasMessage(@RequestHeader("token") String token) {
        Boolean hasMessage = userService.hasMessage(token);
        return hasMessage;
    }

    @Autowired
    private ChatService chatService;

    /*
     *   Get All Chats
     * */
    @GetMapping("/getAllChats")
    public List<Chat> getChatList() {
        List<Chat> chatList = chatService.getAllChats();
        for (int i = 0; i < chatList.size(); i++) {
            System.out.println(chatList.get(i).getMessageDate());
        }
        return chatList;
    }


    /*
     *   Get Message List
     * */
//    @GetMapping("/getMessageContacts")
//    public List<User> messageList(@RequestHeader("token") String token){
//       List<User> contactUsers =  userService.getMessageContacts(token);
//       return contactUsers;
//    }
}
