package am.egs.exam.controller;

import am.egs.exam.model.dto.ImageDto;
import am.egs.exam.service.ImageService;
import am.egs.exam.util.exceptions.AccessDeniedException;
import am.egs.exam.util.exceptions.NullPointerException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private ImageService imageService;

    /*
     *  Add Image
     * */
//    @PostMapping("/addImage")
//    public ResponseEntity add(@RequestBody MultipartFile file,
//                              @RequestHeader("token") String token,
//                              @RequestParam("vinCode") String vinCode) throws IOException {
//
//        try {
//            imageService.add(file, token, vinCode);
//        } catch (NotFoundException e) {
//            return new ResponseEntity("Car isn't exist !!!", HttpStatus.valueOf(409));
//        } catch (AccessDeniedException e) {
//            return new ResponseEntity("This vin code isn't your !!!", HttpStatus.valueOf(400));
//        } catch (NullPointerException e) {
//            return new ResponseEntity("there is no car with this vin code !!!", HttpStatus.valueOf(400));
//        }
//
//        return ResponseEntity.ok().build();
//    }
//
//
//    /*
//     *   Get Image
//     * */
//    @GetMapping("/getImage")
//    public ResponseEntity getFile(@RequestParam("vinCode") String vinCode) throws IOException {
//        ImageDto imageDto = imageService.getFile(vinCode);
//        return ResponseEntity.ok(imageDto);
//    }


}
