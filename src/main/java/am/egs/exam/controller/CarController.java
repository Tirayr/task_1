package am.egs.exam.controller;


import am.egs.exam.model.dto.CarDto;
import am.egs.exam.model.entity.Car;
import am.egs.exam.service.CarService;
import am.egs.exam.service.ImageService;
import am.egs.exam.util.exceptions.AccessDeniedException;
import am.egs.exam.util.exceptions.DuplicateException;
import am.egs.exam.util.exceptions.NullPointerException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private ImageService imageService;

    /*
     *  Add Car
     * */
    @PostMapping("/add")
    public ResponseEntity add(@RequestBody Car car,
                              @RequestHeader("token") String token) {
        try {
            carService.add(car, token);
        } catch (NotFoundException e) {
            return new ResponseEntity("car not found", HttpStatus.valueOf(400));
        } catch (DuplicateException e) {
            return new ResponseEntity("there is car with this vin code", HttpStatus.valueOf(409));
        }
        return ResponseEntity.ok().build();
    }


    /*
     *   Get All
     * */
    @GetMapping("/getAll")
    List<CarDto> hindAll() {
        return carService.getAllCars();
    }

    /*
    *   Get My Cars
    * */
    @GetMapping("/myCars")
    List<Car> findMyCars(@RequestHeader("token") String token) {
        return carService.getMyCars(token);
    }


    /*
     *   Get Car By Marka
     * */
    @GetMapping("/getByMarka")
    public ResponseEntity getByMarka(@RequestParam("marka") String marka) {
        Car car = carService.getByMarka(marka);
        return ResponseEntity.ok(car);
    }

    /*
     *   Get Car By Model
     * */
    @GetMapping("/getByModel")
    public ResponseEntity getByModel(@RequestParam("model") String model) {
        Car car = carService.getByModel(model);
        return ResponseEntity.ok(car);
    }

    /*
     *   Get Car By Creation Year
     * */
    @GetMapping("/byCreationYear")
    public ResponseEntity getByCreationYear(@RequestParam("createYear") int createYear) {
        Car car = carService.getByCreatYear(createYear);
        return ResponseEntity.ok(car);
    }


    /*
     *   Get Car List By Marka, Model, Year
     * */
    @GetMapping("/getCarList")
    public List<Car> FindCarList(@RequestParam("marka") String marka,
                                 @RequestParam("model") String model,
                                 @RequestParam("createYear") String createYear) {
        if (model == null && createYear == null && marka != null) {
            findAllByMarka(marka);
        }
        if (model == null && marka != null && createYear != null) {
            findAllByMarkaAndCreationYear(marka, createYear);
        }
        if (createYear == null && model != null && marka != null) {
            findAllByMarkalAndModel(marka, model);
        }
        if (marka == null && createYear == null && model != null) {
            findAllByModel(model);
        }
        if (marka == null && createYear != null && model != null) {
            findAllByModellAndCreationYear(model, createYear);
        }
        if (marka == null && model == null && createYear != null) {
            findAllByCreationYear(createYear);
        }
        return null;
    }


    /*
     *  Get Car List By Marka
     * */
    @GetMapping("/searchMarka")
    public List<Car> findAllByMarka(@RequestParam("marka") String marka) {
        return carService.findAllByMarka(marka);
    }

    /*
     *  Get Car List By Model
     * */
    @GetMapping("/searchModel")
    public List<Car> findAllByModel(@RequestParam("model") String model) {
        return carService.findAllByModel(model);
    }

    /*
     *   Get Car List By Creation Year
     * */
    @GetMapping("/searchByYear")
    public List<Car> findAllByCreationYear(@RequestParam("createYear") String creationYear) {
        return carService.findAllByCreationYear(creationYear);
    }

    /*
     *   Get Car List By Marka And Creation Year
     * */
    @GetMapping("/searchByMarkaYear")
    public List<Car> findAllByMarkaAndCreationYear(@RequestParam("marka") String marka,
                                                   @RequestParam("createYear") String creationYear) {
        return carService.findAllByMarkaAndCreationYear(marka, creationYear);
    }

    /*
     *   Get Car List By Marka And Model
     * */
    @GetMapping("/searchModelMarka")
    public List<Car> findAllByMarkalAndModel(@RequestParam("marka") String marka,
                                             @RequestParam("model") String model) {
        return carService.findAllByMarkaAndModel(marka, model);
    }

    /*
     *  Get Car List By Model And Creation Year
     * */
    @GetMapping("/searchByModelYear")
    public List<Car> findAllByModellAndCreationYear(@RequestParam("model") String model,
                                                    @RequestParam("createYear") String createYear) {
        return carService.findAllByModelAndCreationYear(model, createYear);
    }

    /*
     *   Get Car List By All Params
     * */
    @GetMapping("/searchAllParams")
    public List<Car> findAllByAllParams(@RequestParam("marka") String marka,
                                        @RequestParam("model") String model,
                                        @RequestParam("createYear") String createYear) {
        return carService.findAllByAllParams(marka, model, createYear);
    }

    /*
    *   Add Image
    * */
    @PostMapping("/addImage")
    public ResponseEntity add(@RequestBody List<MultipartFile> files,
                              @RequestHeader("token") String token,
                              @RequestParam("vinCode") String vinCode) throws IOException {
        try {
            imageService.add(files, token, vinCode);
        } catch (NotFoundException e) {
            return new ResponseEntity("Car isn't exist !!!", HttpStatus.valueOf(409));
        } catch (AccessDeniedException e) {
            return new ResponseEntity("This vin code isn't your !!!", HttpStatus.valueOf(400));
        } catch (NullPointerException e) {
            return new ResponseEntity("there is no car with this vin code !!!", HttpStatus.valueOf(400));
        }


        return ResponseEntity.ok().build();
    }

    /*
     *   Get Image
     * */
    @GetMapping("/getImage")
    public ResponseEntity getFile(@RequestParam("vinCode") String vinCode) throws IOException {
//        ImageDto imageDto = imageService.getFile(vinCode);
//        return ResponseEntity.ok(imageDto);

        List<String> images = imageService.getFile(vinCode);
        return ResponseEntity.ok(images);
    }


}
