package am.egs.exam.controller;

import am.egs.exam.service.AdminService;
import am.egs.exam.util.exceptions.AccessDeniedException;
import am.egs.exam.util.exceptions.RoleNotFoundException;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    /*
     *    DELETE USER
     * */
    @DeleteMapping("/delete")
    public ResponseEntity delete(@RequestHeader("token") String token,
                                 @RequestHeader("email") String email) {

        System.out.println(email);
        try {
            adminService.delete(email);
        }catch (NotFoundException e){
            return new ResponseEntity("user doesn't exist !!!", HttpStatus.valueOf(400));

       }
//        catch (AccessDeniedException e){
//            return new ResponseEntity("you can't delete admin !!!", HttpStatus.valueOf(409));
//
//        }
        catch (RoleNotFoundException e) {
            return new ResponseEntity("user have not roles", HttpStatus.valueOf(400));
        }
        return ResponseEntity.ok().build();
    }
}
