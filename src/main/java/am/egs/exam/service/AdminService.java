package am.egs.exam.service;

import am.egs.exam.util.exceptions.RoleNotFoundException;
import javassist.NotFoundException;

public interface AdminService {
    void delete(String email) throws NotFoundException, RoleNotFoundException;

}
