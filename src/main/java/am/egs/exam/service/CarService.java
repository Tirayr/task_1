package am.egs.exam.service;

import am.egs.exam.model.dto.CarDto;
import am.egs.exam.model.entity.Car;
import am.egs.exam.util.exceptions.DuplicateException;
import javassist.NotFoundException;

import java.util.List;

public interface CarService {

    Car getByMarka(String marka);

    Car getByModel(String model);

    Car getByCreatYear(int year);


    List<CarDto> getAllCars();

    List<Car> findAllByMarka(String marka);

    List<Car> findAllByModel(String marka);

    List<Car> findAllByCreationYear(String creationYear);

    List<Car> findAllByMarkaAndCreationYear(String marka, String creationYear);

    List<Car> findAllByMarkaAndModel(String marka, String model);

    List<Car> findAllByModelAndCreationYear(String model, String createYear);


    List<Car> findAllByAllParams(String marka, String model, String createYear);


    List<Car> getMyCars(String token);


    void add(Car car, String token) throws NotFoundException, DuplicateException;
}
