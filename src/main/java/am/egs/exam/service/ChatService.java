package am.egs.exam.service;

import am.egs.exam.model.entity.Chat;
import javassist.NotFoundException;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface ChatService {
    void setNotification(String mailOfRecipientUser) throws NotFoundException;

    void saveMessage(String senderEmail, String header, String content, Date now) throws IOException, NotFoundException;

    List<Chat> getAllChats();

}
