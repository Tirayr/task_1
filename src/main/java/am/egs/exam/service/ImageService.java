package am.egs.exam.service;

import am.egs.exam.util.exceptions.AccessDeniedException;
import am.egs.exam.util.exceptions.NullPointerException;
import javassist.NotFoundException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ImageService {


    void add(List<MultipartFile> files, String token, String vinCode) throws IOException, NotFoundException, AccessDeniedException, NullPointerException;


    List<String> getFile(String vinCode) throws IOException;

}
