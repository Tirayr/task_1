package am.egs.exam.service.impl;

import am.egs.exam.model.entity.User;
import am.egs.exam.repository.UserRepository;
import am.egs.exam.service.AdminService;
import am.egs.exam.util.exceptions.RoleNotFoundException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

@Service

public class AdminServiceImpl implements AdminService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void delete(String email) throws NotFoundException, AccessDeniedException, RoleNotFoundException {

        User user = userRepository.getUserByEmail(email);
        System.out.println(user.toString());
        boolean isAdmin = user.getRoles().stream().anyMatch(item -> item.getRole().equals("ROLE_ADMIN"));


        if (user == null) {
            throw new NotFoundException("user doesn't exist !!!");
        }


        if (user.getRoles() == null) {
            throw new RoleNotFoundException("user have not roles");
        }

        if (!isAdmin) {
            userRepository.delete(user);
        } else
            throw new AccessDeniedException("you can't delete admin !!!");
    }
}
