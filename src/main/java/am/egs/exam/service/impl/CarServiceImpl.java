package am.egs.exam.service.impl;

import am.egs.exam.config.jwt.JwtProvider;
import am.egs.exam.model.dto.CarDto;
import am.egs.exam.model.entity.Car;
import am.egs.exam.repository.CarRepository;
import am.egs.exam.service.CarService;
import am.egs.exam.util.exceptions.DuplicateException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private JwtProvider jwtProvider;



    /*
     *   Add Car
     * */
    @Override
    public void add(Car car, String token) throws NotFoundException, DuplicateException {
        if (car == null){
            throw new NotFoundException("car not found !!!");
        }
        if (carRepository.findCarByvinCode(car.getVinCode()) != null){
            throw new DuplicateException("there is car with this vin code !!!");
        }
        String email = jwtProvider.getEmail(token);
        car.setOwnerContact(email);
        carRepository.save(car);

    }

    /*
     *   Get By Marka
     * */
    @Override
    public Car getByMarka(String marka) {
        if (marka == null || marka.equals("")) {
            throw new NullPointerException("car is not exics");
        }
        Car car = carRepository.findCarByMarka(marka);
        return car;
    }

    /*
     *  Get By Model
     * */
    @Override
    public Car getByModel(String model) {
        if (model == null || model.equals("")) {
            throw new NullPointerException("car is nor exics");
        }
        Car car = carRepository.findCarByModel(model);
        return car;
    }


    /*
     *  Get By Creation Year
     * */
    @Override
    public Car getByCreatYear(int year) {
        if (year == 0) {
            throw new NullPointerException("car is nor exics");
        }
        Car car = carRepository.findCarByCreateYear(year);
        return car;
    }


    /*
     *   Get All
     * */
    @Override
    public List<CarDto> getAllCars() {
        List<Car> cars = carRepository.findAll();
        List<CarDto> carDtoList = new ArrayList<>();
        for (int i = 0; i < cars.size(); i++) {
            CarDto carDto = new CarDto();
            carDto.setMarka(cars.get(i).getMarka());
            carDto.setModel(cars.get(i).getModel());
            carDto.setOwnerContact(cars.get(i).getOwnerContact());
            carDto.setCreateYear(cars.get(i).getCreateYear());
            carDto.setVinCode(cars.get(i).getVinCode());
            carDtoList.add(carDto);
        }
        return carDtoList;
    }

    /*
     *   Get My Cars
     * */
    @Override
    public List<Car> getMyCars(String token) {
        String email = jwtProvider.getEmail(token);
        return carRepository.findCarByUserEmail(email);
    }

    /*
     *   Get All Cars By Marka
     * */
    @Override
    public List<Car> findAllByMarka(String marka) {
        return carRepository.findAllByMarka(marka);
    }


    /*
     *   Get All Cars By Model
     * */
    @Override
    public List<Car> findAllByModel(String model) {
        return carRepository.findAllByModel(model);
    }

    /*
     *   Get All Cars By Creation Year
     * */
    @Override
    public List<Car> findAllByCreationYear(String creationYear) {
        return carRepository.findAllByCreateYear(creationYear);
    }

    /*
     *  Get All Cars By Marka And Creation Year
     * */
    @Override
    public List<Car> findAllByMarkaAndCreationYear(String marka, String creationYear) {
        return carRepository.findByMarkaAndCreationYear(marka, creationYear);
    }

    /*
     *  Get All Cars By Marka And Model
     * */
    @Override
    public List<Car> findAllByMarkaAndModel(String marka, String model) {
        return carRepository.findByMarkaAndModel(marka, model);
    }

    /*
     *  Get All Cars By model And Creation Year
     * */
    @Override
    public List<Car> findAllByModelAndCreationYear(String model, String createYear) {
        return carRepository.findByModelAndCreationYear(model, createYear);
    }

    /*
     *   Get All Cars By All Params
     * */
    @Override
    public List<Car> findAllByAllParams(String marka, String model, String createYear) {
        return carRepository.findAllByAllParams(marka, model, createYear);
    }

}
