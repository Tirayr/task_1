package am.egs.exam.service.impl;

import am.egs.exam.model.entity.Chat;
import am.egs.exam.model.entity.User;
import am.egs.exam.repository.ChatRepository;
import am.egs.exam.repository.UserRepository;
import am.egs.exam.service.ChatService;
import am.egs.exam.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ChatServiceImpl implements ChatService {


    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;


    /*
    *  Set Notification
    * */
    @Override
    public void setNotification(String mailOfRecipientUser) throws NotFoundException {
        User user = userService.getByEmail(mailOfRecipientUser);
        if (user == null) {
            throw new NotFoundException("user doesn't exist !!!");
        } else {
            user.setNotification(true);
            userRepository.save(user);
        }
    }


    /*
    *   Save Message
    * */
    @Override
    public void saveMessage(String senderEmail, String header, String content, Date now){
        Chat chat = new Chat();
        chat.setSenderEmail(senderEmail);
        chat.setHeader(header);
        chat.setContent(content);
        String messageDate = now.getYear() + "-" + now.getMonth() + "-" + now.getDay() + "\n"
                + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
        chat.setMessageDate(messageDate);
        chatRepository.save(chat);
    }

    @Override
    public List<Chat> getAllChats() {
        return chatRepository.findAll();
    }


//    @Override
//    public void saveMessage(String senderEmail, String header, String content) throws IOException, NotFoundException {
//        User senderUser = userService.getByEmail(senderEmail);
//        User recipentUser = userService.getByEmail(header);
//
//        String usersDialog = senderEmail + "_" + header;
//        List<File> files = new ArrayList<>();
//
//        String parentFolderPath = "C:\\Users\\tirayrs\\Desktop\\Chat\\";
//        File parentDir = new File(parentFolderPath);
//        if (!parentDir.exists()) {
//            parentDir.mkdirs();
//        }
//
//        if (parentDir.isDirectory()){
//            System.out.println(parentDir.getName());
//            System.out.println(parentDir.length());
//            if (parentDir.length() == 0) {
//                File file = new File(parentFolderPath + usersDialog + ".txt");
//                System.out.println(file.getName());
//                String contentToWriteInFile = content + "\n \n";
//                FileOutputStream fos = new FileOutputStream(file, true);
//                fos.write(contentToWriteInFile.getBytes());
//                fos.close();
//                files.add(file);
//            } else {
//                for (int i = 0; i < files.size(); i++) {
//                    if (files.get(i).getName().contains(senderEmail) && files.get(i).getName().contains(header)) {
//                        String contentToWriteInFile = content + "\n \n";
//                        FileOutputStream fos = new FileOutputStream(files.get(i), true);
//                        fos.write(contentToWriteInFile.getBytes());
//                        fos.close();
//                    } else {
//                        File file = new File(parentFolderPath + usersDialog + ".txt");
//                        String contentToWriteInFile = content + "\n \n";
//                        FileOutputStream fos = new FileOutputStream(file, true);
//                        fos.write(contentToWriteInFile.getBytes());
//                        fos.close();
//                        files.add(file);
//                    }
//                }
//            }
//        }
//    }


}
