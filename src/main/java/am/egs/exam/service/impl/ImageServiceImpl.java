package am.egs.exam.service.impl;

import am.egs.exam.config.jwt.JwtProvider;
import am.egs.exam.model.dto.ImageDto;
import am.egs.exam.model.entity.Car;
import am.egs.exam.model.entity.Image;
import am.egs.exam.model.entity.User;
import am.egs.exam.repository.CarRepository;
import am.egs.exam.repository.ImageRepository;
import am.egs.exam.service.ImageService;
import am.egs.exam.service.UserService;
import am.egs.exam.util.exceptions.AccessDeniedException;
import am.egs.exam.util.exceptions.NullPointerException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private UserService userService;


    /*
     *  Add Image
     * */
    @Override
    public void add(List<MultipartFile> files, String token, String vinCode) throws IOException, NotFoundException, AccessDeniedException, NullPointerException {

        String email = jwtProvider.getEmail(token);
        User user = userService.getByEmail(email);
        String name = user.getName();
        String surname = user.getSurname();

        String filePath = "C:\\Users\\tirayrs\\Desktop\\Images\\" + name + "_" + surname + "\\";
        File dir = new File(filePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        List<Image> imageList = new ArrayList<>();


        for (int i = 0; i < files.size(); i++) {

            File img = new File(filePath + files.get(i).getOriginalFilename());
            FileOutputStream fos = new FileOutputStream(img);
            fos.write(files.get(i).getBytes());
            fos.close();

            String fullPath = filePath + img.getName();
            Image image = new Image(fullPath);
            imageRepository.save(image);
            imageList.add(image);

        }

        Car car = carRepository.findCarByvinCode(vinCode);
        if (car == null) {
            throw new NullPointerException("there is no car with this vin code !!!");
        } else {
            if (!car.getOwnerContact().equals(email)) {
                throw new AccessDeniedException("you can't add image in other acount !!!");
            } else {
                if (!car.getVinCode().equals(vinCode)) {
                    throw new NotFoundException("Car isn't exist !!!");
                } else {
                    car.setImages(imageList);
                    carRepository.save(car);
                }

            }
        }

    }

    /*
     *   Get File
     * */
    @Override
    public List<String> getFile(String vinCode) throws IOException {
        Car car = carRepository.findCarByvinCode(vinCode);
        List<Image> images = car.getImages();
      //  ImageDto imageDto = new ImageDto();
        List<String> imageStringList = new ArrayList<>();


// create file with path from db
        for (int i = 0; i < images.size(); i++) {
            String filePath = images.get(i).getFilePath();
            File file = new File(filePath);
            // read all bytes from file and put that in byte[]
            byte[] bytes = Files.readAllBytes(Paths.get(file.getPath()));
            // encode byte[] via Base64.encoder and convert to string
            String imageString = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(bytes);
            imageStringList.add(imageString);
        }
        return imageStringList;
    }
}
