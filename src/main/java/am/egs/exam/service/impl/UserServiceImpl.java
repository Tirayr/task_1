package am.egs.exam.service.impl;

import am.egs.exam.config.jwt.JwtProvider;
import am.egs.exam.model.Message;
import am.egs.exam.model.dto.UserDto;
import am.egs.exam.model.entity.Car;
import am.egs.exam.model.entity.Role;
import am.egs.exam.model.entity.User;
import am.egs.exam.model.user_state.State;
import am.egs.exam.repository.CarRepository;
import am.egs.exam.repository.UserRepository;
import am.egs.exam.service.UserService;
import am.egs.exam.util.GenerateCode;
import am.egs.exam.util.MailSenderClient;
import am.egs.exam.util.exceptions.DuplicateException;
import am.egs.exam.util.exceptions.WrongCodeException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private MailSenderClient mailSenderClient;

    @Autowired
    private CarRepository carRepository;


    /*
     *  User Registration
     * */
    @Override
    public void add(User user) throws DuplicateException, MessagingException, NotFoundException {
        if (user == null) {
            throw new NotFoundException("user not found !!!");
        }

        if (userRepository.getUserByEmail(user.getEmail()) != null) {
            throw new DuplicateException("user is exist !!!");
        }

        Role role = new Role("ROLE_USER");
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setRoles(roles);
        user.setState(State.OFFLINE);

        user.setCode(GenerateCode.generateCode());
        mailSenderClient.send(user.getEmail(), "VerificationMessage", GenerateCode.getVerificationMessage(user));

        userRepository.save(user);
    }

    /*
     *  Get User By Email
     * */
    @Override
    public User getByEmail(String email) throws NotFoundException {
        User user = userRepository.getUserByEmail(email);
        if (user == null) {
            throw new NotFoundException("user doesn't exist !!!");
        } else
            return userRepository.getUserByEmail(email);
    }


    /*
     *  Get Token By Email and Roles
     * */
    @Override
    public String getToken(String email, List<Role> roles) {
        String token = jwtProvider.createToken(email, roles);
        return token;
    }


    /*
     *  Get User By Token
     * */
    @Override
    public Boolean getUserByToken(String token) {
        String email = jwtProvider.getEmail(token);
        User user = userRepository.getUserByEmail(email);

        user.setState(State.ONLINE);
        userRepository.save(user);

        List<Role> roles = user.getRoles();
        List<String> roleValues = new ArrayList<>();
        for (int i = 0; i < roles.size(); i++) {
            roleValues.add(roles.get(i).getRole());
        }

        if (roleValues.contains("ROLE_ADMIN")) {
            return true;
        } else {
            return false;
        }
    }


    /*
     *  Verification Code
     * */
    @Override
    public void verify(String email, String code) throws NotFoundException, WrongCodeException {
        User user = userRepository.getUserByEmail(email);

        if (user == null) {
            throw new NotFoundException("There is no user with this email !!!");
        }

        if (!user.getCode().equals(code)) {
            throw new WrongCodeException("Wrong verification code !!!");
        }
        user.setCode(null);
        userRepository.save(user);
    }

    /*
    *   Get All Users
    * */
    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    /*
    *  Update User
    * */
    @Override
    public void update(User user, String token) throws AccessDeniedException, NotFoundException {
        if (user == null) {
            throw new NotFoundException("user doesn't exist !!!");
        }

        String userEmail = jwtProvider.getEmail(token);

        if (!user.getEmail().equals(userEmail)) {
            throw new AccessDeniedException("you can't update data of other users");
        }

        User user1 = userRepository.getUserByEmail(user.getEmail());
        user1.setName(user.getName());
        user1.setSurname(user.getSurname());
        user1.setAge(user.getAge());
        user1.setEmail(user.getEmail());
        user1.setPassword(user.getPassword());
        userRepository.save(user1);
    }

    /*
    *   Find UserDto by token
    * */
    @Override
    public UserDto findUserByEmail(String token) {
        UserDto userDto = new UserDto();
        String email = jwtProvider.getEmail(token);
        User user = userRepository.getUserByEmail(email);
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setEmail(user.getEmail());

        return userDto;
    }

    @Override
    public UserDto getUserByCarVinCode(String vinCode) {
        Car car = carRepository.findCarByvinCode(vinCode);
        String email = car.getOwnerContact();
        User user = userRepository.getUserByEmail(email);
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setEmail(user.getEmail());
        return userDto;
    }


    /*
    *   Log out
    * */
    @Override
    public void logOut(String token) {
        String email = jwtProvider.getEmail(token);
        User user = userRepository.getUserByEmail(email);
        user.setState(State.OFFLINE);
        userRepository.save(user);
    }


    /*
    *   Has Message
    * */
    @Override
    public Boolean hasMessage(String token) {
        String email = jwtProvider.getEmail(token);
        User user = userRepository.getUserByEmail(email);
        if (user.isNotification() == true){
            return true;
        } else {
            return false;
        }
    }


    /*
    *   Get Contact Users
    * */
//    @Override
//    public List<User> getMessageContacts(String token) {
//
//    }


}
