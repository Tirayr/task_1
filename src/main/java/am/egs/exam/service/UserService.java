package am.egs.exam.service;

import am.egs.exam.model.dto.UserDto;
import am.egs.exam.model.entity.Role;
import am.egs.exam.model.entity.User;
import am.egs.exam.util.exceptions.DuplicateException;
import am.egs.exam.util.exceptions.WrongCodeException;
import javassist.NotFoundException;

import javax.mail.MessagingException;
import java.util.List;


public interface UserService {
    void add(User user) throws DuplicateException, MessagingException, NotFoundException;

    User getByEmail(String email) throws NotFoundException;

    String getToken(String email, List<Role> roles);

    Boolean getUserByToken(String token);

    void verify(String email, String code) throws NotFoundException, WrongCodeException;

    List<User> getAllUsers();


    void update(User user, String token) throws NotFoundException;

    UserDto findUserByEmail(String token);

    UserDto getUserByCarVinCode(String vinCode);

    void logOut(String token);

    Boolean hasMessage(String token);

  //  List<User> getMessageContacts(String token);
}
