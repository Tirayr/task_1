package am.egs.exam.repository;

import am.egs.exam.model.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Car findCarByMarka(String marka);

    Car findCarByModel(String model);

    Car findCarByCreateYear(int year);

    Car findCarByvinCode(String vinCode);

    /*
     *  Get By Owner Contact
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.owner_contact = :owner_contact",
            nativeQuery = true)
    List<Car> findCarByUserEmail(@Param("owner_contact") String owner_contact);

    /*
     *  Search By Marka
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.marka like %:marka% ",
            nativeQuery = true)
    List<Car> findAllByMarka(@Param("marka") String marka);

    /*
     *   Search By Model
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.model like %:model%",
            nativeQuery = true)
    List<Car> findAllByModel(@Param("model") String model);

    /*
     *   Search By Creation Year
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.create_year like %:create_year%",
            nativeQuery = true)
    List<Car> findAllByCreateYear(@Param("create_year") String create_year);

    /*
     *   Search By Marka And Creation Year
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.marka like %:marka% and car.create_year like %:create_year%",
            nativeQuery = true)
    List<Car> findByMarkaAndCreationYear(@Param("marka") String marka,
                                         @Param("create_year") String create_year);


    /*
     *   Search By Marka And Model
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.marka like %:marka% and car.model like %:model%",
            nativeQuery = true)
    List<Car> findByMarkaAndModel(@Param("marka") String marka,
                                  @Param("model") String model);

    /*
     *   Search By Model And Creation Year
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.model like %:model% and car.create_year like %:create_year%",
            nativeQuery = true)
    List<Car> findByModelAndCreationYear(@Param("model") String model,
                                         @Param("create_year") String create_year);

    /*
     *   Search By All PArams
     * */
    @Query(value = "SELECT * FROM exam.car WHERE car.marka LIKE %:marka% " +
            "and car.model LIKE %:model% and car.create_year LIKE %:create_year%",
            nativeQuery = true)
    List<Car> findAllByAllParams(@Param("marka") String marka,
                                 @Param("model") String model,
                                 @Param("create_year") String create_ear);


}
