package am.egs.exam.repository;

import am.egs.exam.model.Message;
import am.egs.exam.model.entity.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatRepository extends JpaRepository<Chat, Long> {
}
