package am.egs.exam.repository;


import am.egs.exam.model.Message;
import am.egs.exam.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User getUserByEmail(String email);

    User findUserById(Long id);

    /*
     *   Search By Marka And Creation Year
//     * */
//    @Query(value = "SELECT * FROM exam.car WHERE car.marka like %:marka% and car.create_year like %:create_year%",
//            nativeQuery = true)
//    List<Car> findByMarkaAndCreationYear(@Param("marka") String marka,
//                                         @Param("create_year") String create_year);

//    @Query(value = "SELECT content FROM exam.message WHERE header = :email",
//    nativeQuery = true)
//    List<Message> getMessageListByEmail(@Param("email")String email);

}
